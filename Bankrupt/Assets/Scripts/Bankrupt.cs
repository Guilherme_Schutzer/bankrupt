﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bankrupt
{

    public Text OutputText;
    private BankruptPlayer[] players;
    private int turnLimit = 1000;
    private int lastTurn;
    private BankruptPlayer winner;
    private List<Property> board;
    private int boardSize = 0;

    Bankrupt(string filename, BankruptPlayer[] players)
    {
        board = new List<Property>;
        string[] lines = System.IO.File.ReadAllLines(filename);
        foreach (string line in lines)
        {
            string[] splitted = line.Split(' ');
            int sellValue;
            int rentValue;
            Int32.TryParse(splitted[0], out sellValue);
            Int32.TryParse(splitted[1], out rentValue);
            board.Add(new Property(sellValue, rentValue));
            this.boardSize++;
        }

        this.players = players;
        System.Random rand = new System.Random();
        for (int i = 0; i < players.Length; i++)
        {
            int j = rand.Next(i, players.Length);
            BankruptPlayer temp = this.players[i];
            this.players[i] = this.players[j];
            this.players[j] = temp;
        }
        foreach (BankruptPlayer player in this.players)
        {
            player.SetCoins(300);
            player.SetPos(0);
        }
    }

    public void StartGame()
    {
        int turn = 1;
        bool end = false;
        while (!end)
        {
            end = TurnCycle(turn);
            turn++;
        }
    }

    private bool TurnCycle(int turn)
    {
        if (turn > this.turnLimit)
        {
            this.winner = TimeWinner();
            return true;
        }

        System.Random rand = new System.Random();
        foreach (BankruptPlayer player in this.players)
        {
            int dice = rand.Next(1, 6);
            int newPos = player.Walk(dice, this.board.Length);
        }



        return false;
    }

    private BankruptPlayer TimeWinner()
    {

    }

}
